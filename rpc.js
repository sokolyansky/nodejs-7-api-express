'use strict';

const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const RPC = require('./methodsRPC');
const db = require('./db.js'); // подключили db - абстрактную базу данных
const port = process.env.PORT || 3000;


// var name, score, id;  // глобальные переменные недопустимы, т.к. следующий request перепишет данные предыдущего.

// раздача статики через express
app.use(express.static(__dirname + '/publicRPC')); // в данном случае берет index.html Можно ли сделать, чтобы брал с произвольнвм названием?
// для универсализации приходящего запроса
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/rpc', function (req, res, next) { // у предварительной middleware - 3 параметра
         // у middleware, обрабатывающей ошибки и расположенной ниже роутов - 4 параметра
  const id = req.params.id;

  if(id >= db.length) {
    throw new Error(`User ${req.params.id} hasn't existed yet`); // как передать именно это сообщение на client
  }

  next();
});

// у подхода rpc approach - only 1 route в отличии от REST
app.post('/rpc', function (req, res) {


  const method = RPC[req.body.method];
  //console.log(req.body.params);
  // const {name, score} = req.body.params;
  method(req.body.params, req.body.id, function (error, result) {  // [TypeError: method is not a function]
    if (error) throw new Error('Bad request');
    // res.setHeader('Content-Type', 'application/json');
    // res.send(result);  // здесь необходимо вручную устанавливать заголовки
    // console.log(result);  // св-во id=undefined - автоматически отбрасывается (т.к. undefined) на client не передается
    res.json(result); // этот метод сам формирует необходимые заголовки
  });
});


app.use(function (err, req, res, next) {
  console.log(err);   // выводится объект [Error: User 12 hasn't existed yet]
  res.send(err); // почему в client приходит пустой объект?
});

app.listen(port, function () {
  console.log('Start HTTP server on %d port', port);
});