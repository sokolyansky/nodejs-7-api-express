const db = require('./db.js'); // подключили db - абстрактную базу данных

const RPC = {
  getAll({name, score} = {}, id = null, handle) {
    handle(null, rpcResponse(db));
  },

  get({name, score} = {}, id, handle) {
    handle(null, rpcResponse(db[id], id));
  },

  post({name, score}, id = null, handle) {
    db.push({
      name,
      score
    });
    // console.log(typeof db);  // object  - ожидаю array
    handle(null, rpcResponse(db[db.length - 1], (db.length - 1)));
  },

  put({name, score}, id, handle) {
    db[id].name = name;
    db[id].score = score;

    handle(null, rpcResponse(db[id], id));
  },

  del({name, score}, id, handle) {
    const out = db[id];
    delete db[id];
    handle(null, rpcResponse(out, id));
  }
};

// private method - скроем реализацию этого метода
function rpcResponse(user, id) {
  const res = {
    jsonrpc: 2.0,
    result: user,
    id     // ES6 - создает ключ с названием переменной и значением равным значению переменной
  };
  return res;
}

module .exports = RPC;