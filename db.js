// Abstract DB
let students = [
  {name: 'Алексей Петров', score: 10},
  {name: 'Ирина Овчинникова', score: 20},
  {name: 'Глеб Стукалов', score: 15},
  {name: 'Виктория Заровская', score: 30},
  {name: 'Алексей Левенец', score: 25},
  {name: 'Тимур Вамуш', score: 40},
  {name: 'Евгений Прочан', score: 50},
  {name: 'Александр Малов', score: 60}
];
module.exports = students;